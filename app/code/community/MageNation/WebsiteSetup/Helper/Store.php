<?php
/**
 * Store.php
 * @author rgranadino
 */
/**
* Store Helper Class
* @author rgranadino
*/
class MageNation_WebsiteSetup_Helper_Store extends MageNation_WebsiteSetup_Helper_Data
{
    /**
     * add website, will update if exists
     * required keys: name, code
     * @param array $data
     */
    public function addWebsite(array $data)
    {
        $requiredKeys = array('name', 'code');
        $this->requireKeys($requiredKeys, $data, 'The following keys are mising from store data: %s');
        
        /* @var $websiteModel Mage_Core_Model_Website */
        $websiteModel   = Mage::getModel('core/website');
        $websiteModel->load($data['code'], 'code');
        
        if (!$websiteModel->getId()) {//value must be null/reset
            $websiteModel->setId(null);
        }
        $websiteModel->setName($data['name']);
        $websiteModel->setCode($data['code']);
        if (isset($data['is_default'])) {
            $websiteModel->setIsDefault($data['is_default']);
        }
        
        $websiteModel->save();
        Mage::dispatchEvent('website_save', array('website' => $websiteModel));
        
        return $websiteModel;
    }
    
    /**
     * add store
     * required keys: website_id, name, root_category_id
     * @param array $data
     * @return Mage_Core_Model_Store_Group
     */
    public function addStore(array $data)
    {
        $requiredKeys = array('website_id', 'name', 'root_category_id');
        $this->requireKeys($requiredKeys, $data, 'Missing data for store setup: %s');
        /* @var $groupModel Mage_Core_Model_Store_Group */
        $groupModel         = Mage::getModel('core/store_group');
        $groupModel->setName($data['name']);
        $groupModel->setRootCategoryId($data['root_category_id']);
        $groupModel->setWebsiteId($data['website_id']);
        $groupModel->save();
        Mage::dispatchEvent('store_group_save', array('group' => $groupModel));
    
        return $groupModel;
    }
    
    /**
     * add store view
     * required keys: group_id, name, code, is_active, webiste_id
     * @param array $data
     * @return Mage_Core_Model_Store
     */
    public function addStoreView(array $data)
    {
        $requiredKeys = array('group_id', 'name', 'code', 'is_active', 'website_id');
        $this->requireKeys($requiredKeys, $data, 'Missing data for store view: %s');
    
        $eventName  = 'store_edit';
        /* @var $storeModel Mage_Core_Model_Store */
        $storeModel = Mage::getModel('core/store');
        $storeModel->load($data['code']);
    
        $storeModel->setGroupId($data['group_id']);
        $storeModel->setName($data['name']);
        $storeModel->setCode($data['code']);
        $storeModel->setIsActive($data['is_active']);
        $storeModel->setWebsiteId($data['website_id']);
    
        if (!$storeModel->getId()) {
            $storeModel->setId(null);
            $eventName = 'store_add';
        }
    
        $storeModel->save();
        Mage::app()->reinitStores();
        Mage::dispatchEvent($eventName, array('store'=>$storeModel));
        return $storeModel;
    }
    
    
}
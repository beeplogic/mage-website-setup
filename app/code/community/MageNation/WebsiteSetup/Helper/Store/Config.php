<?php
/**
 * Store Config Helper
 * @author Rolando Granadino <rgranadino@cause2be.com>
 *
 */
class MageNation_WebsiteSetup_Helper_Store_Config extends MageNation_WebsiteSetup_Helper_Data
{
    /**
     * update a configuration value in the database
     * @param $scope
     * @param $scopeId store ID
     * @param $path
     * @param $value
     * @return MageNation_WebsiteSetup_Helper_Store_Config
     */
    public function updateConfigValue($scope, $scopeId, $path, $value)
    {
        /* @var $resource Mage_Core_Model_Resource */
        $resource   = Mage::getSingleton('core/resource');
        /* @var $write Varien_Db_Adapter_Pdo_Mysql */
        $write      = $resource->getConnection('core_write');
        $configTbl  = $resource->getTableName('core/config_data');
        $write->delete($configTbl, "scope = '{$scope}' AND scope_id='{$scopeId}' AND path='{$path}'");
        $query  =   "INSERT INTO {$configTbl} (scope, scope_id, path, value) VALUES(?, ?, ?, ?);";
        $write->query($query, array($scope, $scopeId, $path, $value));
        return $this;
    }
    /**
     * update store config from a CSV file
     * @param str $filePath
     */
    public function updateFromCsv($filePath)
    {
        $data = $this->parse($filePath);
        if (!$data) {
            return false;
        }
        foreach ($data as $config) {
            if (count($config) < 2) {
                continue;
            }
            if (!$this->arrayKeysExists(array('path','value'), $config)) {
                continue;
            }
            if (!isset($config['scope'])) {
                //set default scope
                $config['scope'] = 'default';
            }
            if (!isset($config['scope_id'])) {
                //set default scope ID
                $config['scope_id'] = 0;
            }
            $this->updateConfigValue($config['scope'], $config['scope_id'], $config['path'], $config['value']);
        }
    }
}
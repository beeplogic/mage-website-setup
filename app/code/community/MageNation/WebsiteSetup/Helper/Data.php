<?php
/**
 * Data.php
 * @author rgranadino
 */
/**
 * Core Data Helper Class
 * @author rgranadino
 */
class MageNation_WebsiteSetup_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * check a given $search array if all $keys in array exist
     * if $missingKeys is provided will populate 
     * it with keys that have not been supplied
     * @param array $keys
     * @param array $search
     * @param array $missingKeys 
     * @return boolean
     */
    public function arrayKeysExists(array $keys, array $search, &$missingKeys=null)
    {
        $return = true;
        foreach ($keys as $key) {
            if (array_key_exists($key, $search) === false) {
                $return = false;
                if (is_array($missingKeys)) {
                    $missingKeys[] = $key;
                }
            }
        }
        return $return;
    }
    /**
     * 
     * check $keys exist in $search array
     * if a key is missing an exception is thrown with provided
     * $errorString, can have %s which will place comma separated list
     * of missing keys
     * @param array $keys
     * @param array $search
     * @param string $errorString
     * @throws Mage_Core_Exception
     */
    public function requireKeys(array $keys, array $search, $errorString)
    {
        $missing = array();
        if (!$this->arrayKeysExists($keys, $search)) {
            $missingList = implode(',', $missing);
            $message = sprintf($errorString, $missingList);
            Mage::throwException($message);
        }
    }
    
    /**
     * parse file, defaults to csv
     * @param str $file
     * @param str $type
     */
    public function parse($file, $type='csv')
    {
        $ret    = false;
        $method = '_parse'.ucwords(trim($type));
        if (is_callable(array($this, $method))) {
            try {
                $ret = $this->$method($file);
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
        return $ret;
    }
    /**
     * parse csv file
     * @param str $file
     */
    protected function _parseCsv($file)
    {
        $freader    = new Varien_Io_File();
        $ret        = array();
        $keys       = false;
        if ($freader != null) {
            $freader->streamOpen($file, 'r', 0664);
            while(($contents = $freader->streamReadCsv()) !== false) {
                if ($keys == false) {
                    $keys   = $contents;
                } else {
                    $populated = array();
                    foreach ($keys as $k=>$v) {
                        if (isset($contents[$k])) {
                            $populated[$v] = $contents[$k];
                        } else {
                            $populated[$v] = '';
                        }
                    }
                    $ret[] = $populated;
                }
            }
            $freader->streamClose();
        }
        return $ret;
    }
    /**
     * parse xml file
     * @param str $file
     * @return Varien_Simplexml_Element
     */
    protected function _parseXml($file)
    {
        if (is_readable($file)) {
            $string = file_get_contents($file);
            $xml = simplexml_load_string($string, 'Varien_Simplexml_Element');
            if ($xml instanceof Varien_Simplexml_Element) {
                return $xml;
            }
        }
        return false;
    }
}
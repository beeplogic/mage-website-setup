<?php
/**
 * 
 * @author Rolando Granadino <rgranadino@magenation.com>
 *
 */
class MageNation_WebsiteSetup_Helper_Catalog_Product_Attribute extends MageNation_WebsiteSetup_Helper_Data
{
    /**
     * product entity type, used as cache
     * @var int|string
     * @see self::getEntityTypeId
     */
    protected $_entityTypeId = null;

    /**
     * add attribute set
     * @param Mage_Eav_Model_Entity_Setup $setup
     * @param string $setName
     * @param string $parentSetId
     * @throws Mage_Core_Exception
     */
    public function addAttributeSet(Mage_Eav_Model_Entity_Setup $setup, $setName, $parentSetId = null)
    {
        if ($parentSetId === null) {
            $parentSetId = $setup->getDefaultAttributeSetId($this->getEntityTypeId());
        }

        if ($this->getAttributeSetByName($setName))
            return $this->getAttributeSetByName($setName);

        /* @var $attributeSet Mage_Eav_Model_Entity_Attribute_Set */
        $attributeSet = Mage::getModel('eav/entity_attribute_set')
        ->setEntityTypeId($this->getEntityTypeId())
        ->setAttributeSetName($setName);

        $attributeSet->validate();
        $attributeSet->save();
        return $attributeSet->initFromSkeleton($parentSetId)->save();
    }

    /**
     *
     * Enter description here ...
     * @param Mage_Eav_Model_Entity_Setup $setup
     * @param string $groupName
     * @param int $setId
     * @param array $attributes
     */
    public function addAttributeGroup(Mage_Eav_Model_Entity_Setup $setup, $groupName, $setId, $attributes = array())
    {
        $setup->addAttributeGroup($this->getEntityTypeId(), $setId, $groupName);
        if (is_array($attributes) && count($attributes)) {
            foreach ($attributes as $attribute) {
                $setup->addAttributeToGroup($this->getEntityTypeId(), $setId, $groupName, $attribute);
            }
        }
    }
    /**
     * get attribute set by name, returns null if not found
     * @param string $name
     * @return Mage_Eav_Model_Entity_Attribute_Set | null
     */
    public function getAttributeSetByName($name, $entityTypeFilter = null)
    {
        /**
         * Set the filter for the product entity type id
         */
        if ($entityTypeFilter == null) {
            $entityTypeFilter = $this->getEntityTypeId();
        }

        /* @var $collection Mage_Eav_Model_Mysql4_Entity_Attribute_Set_Collection */
        $collection = Mage::getModel('eav/entity_attribute_set')
        ->getResourceCollection()
        ->setEntityTypeFilter($entityTypeFilter)
        ->addFieldToFilter('attribute_set_name', $name);
        $set = $collection->getFirstItem();

        if ($set != null && $set->getId() > 0) {
            return $set;
        }

        return null;
    }

    /**
     * gets product type entity type and caches it
     * @return $entityTypeId string|integer
     */
    public function getEntityTypeId()
    {
        if ($this->_entityTypeId === null) {
            $this->_entityTypeId = Mage::getModel('catalog/product')->getResource()->getEntityType()->getId();
        }

        return $this->_entityTypeId;
    }

    /**
     *
     * Enter description here ...
     * @param unknown_type $typeId
     */
    public function setEntityTypeId($typeId)
    {
        $this->_entityTypeId = $typeId;
        return $this;
    }
    /**
     * Add attribute set
     *
     * @param array $attributeSet
     * @return
     * @see self::addAttributeSet
     */
    public function createAttributeSet(Mage_Eav_Model_Entity_Setup $setup, $attributeSet)
    {
        if ($this->arrayKeysExists(array('set_name'), $attributeSet) === false) {
            Mage::throwException($this->__('Attribute Set Name is missing'));
        }

        $setName = trim($attributeSet['set_name']);
        $parentSetId = null;
        if (!empty($attributeSet['parent_set_name'])) {
            $parentSet = $this->getAttributeSetByName(trim($attributeSet['parent_set_name']));
            if ($parentSet != null && $parentSet->getId() > 0) {
                $parentSetId = $parentSet->getId();
            } else {
                Mage::throwException($this->__('Cannot find parent attribute set id'));
            }
        }

        $set = $this->addAttributeSet($setup, $setName, $parentSetId);
        $groupInformation = array();
        if (!empty($attributeSet['groups'])) {
            if (is_string($attributeSet['groups'])) {
                $groupInformation = Mage::helper('core')->jsonDecode(trim($attributeSet['groups']));
            } else if (is_array($attributeSet['groups'])) {
                $groupInformation = $attributeSet['groups'];
            }
        }
        if (is_array($groupInformation) && count($groupInformation)) {
            foreach ($groupInformation as $groupName=>$attributes) {
                $this->addAttributeGroup($setup, $groupName, $set->getId(), $attributes);
            }
        } else {
            Mage::throwException($this->__('Invalid attribute set specified: %s', trim($attributeSet['groups'])));
        }

        return $set;
    }

    /**
     * TODO: clean this method up...
     * Create attribute
     * @param $setup
     * @param $attr
     */
    public function createAttribute(Mage_Eav_Model_Entity_Setup $setup, $attr)
    {
        /**
         * attribute code required
         */
        if (!isset($attr['code']) || empty($attr['code'])) {
            return false;
        }

        /**
         * attribute scope
         */
        switch($attr['scope'] = strtolower(trim($attr['scope']))) {
            case 'global':
                $scope = Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL;
                break;
            case 'website':
                $scope = Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE;
                break;
            default:
                $scope = Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE;
        }

        /**
         * attribute setup
         * @see $setup::_prepareValues
         */
        $attributeInformation = array(
                'label' => $attr['label'],
                'input' => $attr['input'],
                'type' => (!empty($attr['type'])?$attr['type']:'text'),//maps to frontend_input
                'user_defined' => true,
                'unique' => (!empty($attr['unique'])?(int)$attr['unique']:0),
                'required' => (!empty($attr['required'])?(int)$attr['required']:0),
                'searchable' => (!empty($attr['searchable'])?(int)$attr['searchable']:0),
                'filterable' => (!empty($attr['filterable'])?(int)$attr['filterable']:0),
                'filterable_in_search' => (!empty($attr['filterable'])?(int)$attr['filterable']:0),
                'comparable' => (!empty($attr['comparable'])?(bool)$attr['comparable']:false),
                'is_configurable' => (!empty($attr['configurable'])?(bool)$attr['configurable']:false),
                'global' => $scope,
                'visible_on_front' => (!empty($attr['visible_on_front'])?(bool)$attr['visible_on_front']:false),
                'apply_to' => (!empty($attr['apply_to'])?trim($attr['apply_to']):''),
                'position' => (!empty($attr['layered_nav_position'])?(int)$attr['layered_nav_position']:0),
                'used_for_promo_rules' => (!empty($attr['used_for_promo_rules'])?(int)$attr['used_for_promo_rules']:false),
                'is_html_allowed_on_front' => (!empty($attr['html_on_frontend'])) ? (int)$attr['html_on_frontend']: false,
                'note' => ((!empty($attr['note']))?trim($attr['note']):''),
                'sort_order' => (!empty($attr['sort_order'])?(int)$attr['sort_order']:0),
                'visible' => $attr['is_visible'],
                'is_visible' => $attr['is_visible'],
                'used_in_product_listing' => $attr['used_in_product_listing'],
                'note' => (!empty($attr['note'])) ? $attr['note'] :'',
                'used_for_sort_by' => (!empty($attr['used_for_sort_by'])?(int)$attr['used_for_sort_by']:0), 
                );

        /**
         * Scalar value, text, boolean, etc
         */
        if (in_array($attr['input'], array('select', 'multiselect')) === false) {
            // set default
            $default = trim($attr['values']);
            $attributeInformation['default'] = $default;
        } else {
            /**
             * Convert values for select/multiselect
             */
            $option = array();
            $orders = array();
            $values = explode(',', $attr['values']);
            if (count($values)) {
                for ($x = 0; $x < count($values); $x++) {
                    $option['option_'.$x] = array(trim($values[$x]));
                    $orders['option_'.$x] = ($x+1) * 10;
                }
            }

            // unset default
            // FIXME: fix the default for options
            $attributeInformation['default'] = '';
            if (count($option)) {
                $attributeInformation['option'] = array('value' => $option, 'order'=> $orders);
            }
            $attributeInformation['backend'] = 'eav/entity_attribute_backend_array';
        }

        /**
         * checks for source models and backend models and renderer for attributes
         *
         */
        if (!empty($attr['input_renderer'])) {
            $attributeInformation['frontend_input_renderer'] = $attributeInformation['input_renderer'] = trim($attr['input_renderer']);
        }

        if (!empty($attr['backend_model'])) {
            $attributeInformation['backend'] = trim($attr['backend_model']);
        }

        if (!empty($attr['source_model'])) {
            $attributeInformation['source'] = trim($attr['source_model']);
        }

        if (!empty($attr['frontend_model'])) {
            $attributeInformation['frontend'] = trim($attr['frontend_model']);
        }
        return $setup->addAttribute($this->getEntityTypeId(), trim($attr['code']), $attributeInformation);
    }
}
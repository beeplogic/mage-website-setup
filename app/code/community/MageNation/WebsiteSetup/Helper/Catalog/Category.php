<?php
class MageNation_WebsiteSetup_Helper_Catalog_Category extends MageNation_WebsiteSetup_Helper_Data
{
    /**
     * store root categories in memory
     * @var array
     */
    private $_rootCatCache   = array();
    /**
     * return a root category by url key
     * @param str $key
     * @return Mage_Catalog_Model_Category
     */
    public function getRootCategoryByName($name)
    {
        $name   = strtolower($name);
        if (!isset($this->_rootCatCache[$name])) {
            $this->_rootCatCache    = null;
            /* @var $rootCat Mage_Catalog_Model_Category */
            $rootCat                = $this->getCoreRootCategory();
            foreach ($rootCat->getChildrenCategories() as $childCat) {
                /* @var $childCat Mage_Catalog_Model_Category */
                if (strtolower($childCat->getName()) == $name) {
                    $this->_rootCatCache[$name] = $childCat;
                    break;
                }
            }
        }
        return $this->_rootCatCache[$name];
    }
    /**
     * get the first root category in the system
     * on a default/clean install with no other root categories
     * this should be the "Default" category that magento installs
     * during initial setup (id: 2)
     * @return Mage_Catalog_Model_Category
     */
    public function getFirstRootCategory()
    {
        $rootChildCategories = $this->getCoreRootCategory()->getChildrenCategories();
        if (is_array($rootChildCategories)) {//account for flat catalog
            return array_shift($rootChildCategories);
        }
        return $rootChildCategories->getFirstItem();
    }
    /**
     * 
     * @return Mage_Catalog_Model_Category
     */
    public function getCoreRootCategory()
    {
        if (!isset($this->_rootCatCache['core'])) {
            $this->_rootCatCache['core'] = Mage::getModel('catalog/category')->load(Mage_Catalog_Model_Category::TREE_ROOT_ID);
        }
        return $this->_rootCatCache['core'];
    }
    /**
     * add category
     * @param $data
     * @return Mage_Catalog_Model_Category
     */
    public function addCategory($data)
    {
        $category   = $this->getCategoryModel();
        return $category->setData($data)->save();
    }
    /**
     * get a new category model
     * @return Mage_Catalog_Model_Category
     */
    public function getCategoryModel()
    {
        return Mage::getModel('catalog/category');
    }
    /**
     * import categories from XML structure
     * @param str $file
     */
    public function updateFromXml($file)
    {
        /* @var $nodes Varien_Simplexml_Element */
        $nodes = $this->parse($file, 'xml');
        if ($nodes) {
            $defaultCategory = $this->getFirstRootCategory();
            foreach ($nodes as $node) {
                /* @var $node Varien_Simplexml_Element */
                if ($node->getName() == 'category') {
                    $this->_parseCategoryNode($node, $defaultCategory);
                } 
            }
        }
    }
    /**
     * parse category XML node and insert category records
     * @param Varien_Simplexml_Element $node
     * @param Mage_Catalog_Model_Category $parentCategory
     */
    protected function _parseCategoryNode(Varien_Simplexml_Element $node, Mage_Catalog_Model_Category $parentCategory)
    {
        if (!$node->general) {
            return false;
        }
        $name   = (string) $node->general->name;
        $active = (string) $node->general->active;
        $desc   = (string) $node->general->description;
        $title  = (string) $node->general->title;
        $urlKey = (string) $node->general->url_key;
        $isAnchor = (string) $node->general->is_anchor;
        $incMenu  = $node->general->include_in_menu === null ? true : (string)$node->general->include_in_menu;
        $data   = array(
                'name'        => $name,
                'is_active'   => $active,
                'description' => $desc,
                'meta_title'  => $title,
                'url_key'     => $urlKey,
                'is_anchor'   => $isAnchor,
                'path'        => $parentCategory->getPath(),
                'include_in_menu' => $incMenu
        );
        $category = $this->addCategory($data);
        if ($node->categories) {
            foreach ($node->categories->children() as $subCategory) {
                /* @var $subCategory Varien_Simplexml_Element */
                if ($subCategory->getName() == 'category') {
                    $this->_parseCategoryNode($subCategory, $category);
                }
            }
        }
    }
}